const imageUrl = [
    'img1.jpeg',
    'img2.jpg',
    'img3.jpeg',
    'img4.jpg',
    'img5.jpg',
    'img6.jpeg'
]

const arrows = [
    'left-arrow.png',
    'right-arrow.png'
]

function load(){
    const images = document.getElementById('img-container')
    imageUrl.forEach(element => {
        const img = document.createElement('img');
        img.src='../images/' + element;
        img.classList.add('img-url')
        
        images.appendChild(img)
    });
}

window.onload=load;